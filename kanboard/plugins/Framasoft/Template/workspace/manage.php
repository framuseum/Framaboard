<div class="page-header">
    <h2>Gestion espace Framaboard</h2>
</div>

<form method="post" action="<?= $this->url->href('WorkspaceController', 'delete', ['plugin' => 'Framasoft']) ?>" autocomplete="off">
    <?= $this->form->csrf() ?>

    <p class="alert alert-info">
        <strong>
            Vous êtes sur le point de supprimer votre espace Framaboard.
            Vous devez confirmer votre identité en entrant votre mot de passe.
        </strong><br>
        <small>
            Si vous le souhaitez, vous pouvez télécharger au préalable vos données depuis
            « <?= $this->url->link(t('About'), 'ConfigController', 'index') ?> »,
            « Télécharger la base de données ».
        </small>
    </p>

    <?= $this->form->label(t('Password'), 'password') ?>
    <?= $this->form->password('password', $values, $errors, array('required')) ?>

    <div class="form-actions">
        <button type="submit" class="btn btn-red">Supprimer mon espace</button>
    </div>
</form>
