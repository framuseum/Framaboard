/* global KB */

document.addEventListener('DOMContentLoaded', function() {
    // Remove "distant user" input when creating or updating a user.
    // This is the input of "My profile > Edit Authentication"
    var isLdapInput = document.querySelector('#user-section input[name="is_ldap_user"]');
    if (isLdapInput) {
        var isLdapLabel = isLdapInput.parentNode;
        isLdapLabel.remove();
    }

    // This is the input of "Users management > New user"
    // Since it's in a modal and dynamically loaded, we need to remove it on
    // the Kanboard `modal.afterRender` event.
    KB.on('modal.afterRender', function() {
        var isLdapInput = document.querySelector('#modal-box input[name="is_ldap_user"]');
        if (isLdapInput) {
            var isLdapLabel = isLdapInput.parentNode;
            var isLdapHelp = isLdapLabel.nextElementSibling;

            isLdapLabel.remove();
            isLdapHelp.remove();
        }
    });
});
