<?php

require '../common.php';

// Account name of the user
define('ACCOUNT', get_account_name($_SERVER['HTTP_HOST']));

// Real disk path to the account files
define('PATH_ACCOUNT', join_path(PATH_ACCOUNTS, ACCOUNT));

// E-mail address for the "From" header (notifications)
define('MAIL_FROM', SMTP_FROM);

// Database path
define('DB_FILENAME', join_path(PATH_ACCOUNT, 'db.sqlite'));

// Files path
define('FILES_DIR', join_path(PATH_ACCOUNT, 'files/'));

// Test if ACCOUNT exists, redirect otherwise
if (ACCOUNT === false || !file_exists(PATH_ACCOUNT)) {
    header('Location: //' . URL_BASE);
    exit();
}

// Enable API
define('API_AUTHENTICATION_HEADER', 'X-API-Auth');

define('ENABLE_URL_REWRITE', false);

// Enable or disable "X-Frame-Options: DENY" HTTP header
define('ENABLE_XFRAME', false);
