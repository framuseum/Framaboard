# Contributing to Framaboard

## Is it related to Kanboard?

Framaboard is only a thin layout (i.e. the home page and general service) above Kanboard which actually handles most of the work.
So if your contribution is related to Kanboard, please [visit their page on GitHub](https://github.com/kanboard/kanboard).

## It IS related to Framaboard!

OK! In a first place, I ask you to open [a new ticket on Framagit](https://git.framasoft.org/marien.fressinaud/Framaboard/issues).
Any contribution should have a dedicated ticket to facilitate discussion and monitoring. If your question is already listed there, you can simply comment on the already-opened ticket.

### Is it a bug?

Please be as specific as you can: what were you doing? what happened? what was expected? on which browser and operating system? did you have an error message?

It helps us a lot to understand and reproduce your problem.

### Is it a new feature?

Try to be clear in your explanations: we are not in your mind! And please remember that Framaboard isn't destined to be a full-feature software ; Kanboard is.

### Is it a question?

Well… nothing to add, just ask :).

## There is another way…

Framaboard is hosted by the French association Framasoft which relies mostly on donations. You can [make a donation on the dedicated page](https://soutenir.framasoft.org/). Actually, it's the best way to help us :).

And while your donation is checked by your bank, have a look at our "[Dégooglisons Internet](http://degooglisons-internet.org/)" ("De-google-ify Internet") project.
