"use strict";

$(document).ready(function() {
    var account_manual_change = false;

    $('#subdomain').keyup(function() {
        if (account_manual_change) {
            return;
        }

        var subdomain = $('#subdomain').val();
        $('#account').val(subdomain);
    });

    $('#account').change(function() {
        account_manual_change = ($('#account').val() !== '');
    });

    $('[data-toggle="tooltip"]').tooltip();

    $("#play-pause a").on('click', function() {
        if(jQuery(this).children('.glyphicon').hasClass('glyphicon-pause')) {
            jQuery(this).children('.glyphicon').addClass('glyphicon-play').removeClass('glyphicon-pause');
            jQuery(this).attr('title','Lecture');
            jQuery(this).children('.sr-only').text('Lecture');
            jQuery('#carousel-actus').carousel('pause');
        } else {
            jQuery(this).children('.glyphicon').addClass('glyphicon-pause').removeClass('glyphicon-play');
            jQuery(this).attr('title','Pause');
            jQuery(this).children('.sr-only').text('Pause');
            jQuery('#carousel-actus').carousel('cycle');
        };
        return false;
    });

    $('#login').not('.active').hide();
    $('#registration').not('.active').hide();
    $('a[href*=login]').on('click', function() {
        $('#registration').hide();
        $('#login').slideDown();
        return false;
    });
    $('a[href*=registration]').on('click', function() {
        $('#login').hide();
        $('#registration').slideDown();
        return false;
    });
});
