<?php

require '../common.php';
require '../kanboard/vendor/autoload.php';


/**
 * Test if input values are correct.
 *
 * Tested values are:
 *  - subdomain: max 25 chars from \w\-, starts and ends by a-z0-9
 *  - account: max 25 chars from \w\-
 *  - password: not empty
 *  - email: valid email address
 * All values are required.
 *
 * @param array $values input values to validate.
 * @return true if $values are correct, false otherwise.
 */
function values_are_valid($values) {
    if (empty($values['subdomain']) ||
            empty($values['account']) ||
            empty($values['password']) ||
            empty($values['email'])) {
        return 'empty';
    }

    if (!preg_match('/^[a-zA-Z0-9]([\w\-]{0,23}[a-zA-Z0-9])?$/', $values['subdomain'])) {
        return 'subdomain';
    }

    if (!preg_match('/^[\w\-]{0,25}?$/', $values['account'])) {
        return 'account';
    }

    if (strlen($values['password']) <= 0) {
        // Already tested by empty(...) but if one day we want to set
        // a minimal number of chars, we'll just have to change the 0.
        return 'password';
    }

    // There is no "easy" way to validate an email since filter_var does not
    // support emails with accents. We just don't test this value since email
    // is not critical for Framasoft: that's a fair solution.
    // if (!filter_var($values['email'], FILTER_VALIDATE_EMAIL)) {
    //     return 'email';
    // }

    return true;
}

/**
 * Check if an account is already existing.
 *
 * @param string $subdomain the name of the subdomain to check.
 * @return bool True if the account exists, false otherwise.
 */
function check_account_exists($subdomain) {
    $account_path = join_path(PATH_ACCOUNTS, $subdomain);
    if (file_exists($account_path)) {
        return true;
    } else {
        return false;
    }
}


/**
 * Return languages supported by Kanboard.
 *
 * @return array Kanboard languages where keys correspond to browser
 *               languages and values to Kanboard language.
 * @todo check if scandir() returns only directories.
 */
function get_kanboard_languages() {
    $lang_path = join_path(PATH_ROOT, 'kanboard', 'app', 'Locale');
    $langs = array_values(array_diff(
        scandir($lang_path),
        array('..', '.')
    ));
    // English is not present in the Locale directory
    $langs[] = 'en_US';

    $supported_langs = array();
    foreach ($langs as $lang) {
        $short = substr($lang, 0, 2);
        $browser_language = str_replace('_', '-', $lang);
        $supported_langs[$browser_language] = $lang;
        $supported_langs[$short] = $lang;
    }

    return $supported_langs;
}


/**
 * Return language browser preference.
 *
 * Array is sorted by priority according to HTTP_ACCEPT_LANGUAGE string.
 *
 * @return array of prefered languages from browser header.
 * @from http://www.thefutureoftheweb.com/blog/use-accept-language-header
 */
function get_browser_languages() {
    $langs = array();
    if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
        // break up string into pieces (languages and q factors)
        $lang_regex = '/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i';
        preg_match_all($lang_regex, $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);

        if (count($lang_parse[1])) {
            // create a list like "en" => 0.8
            $langs = array_combine($lang_parse[1], $lang_parse[4]);

            foreach ($langs as $lang => $val) {
                // set default to 1 for any without q factor
                $langs[$lang] = $val === '' ? 1 : $val;

                // Take care of short versions.
                // E.g. "en_GB" is not supported by Kanboard but "en" is with
                // the "en_US" version. Still better than using French ;).
                $short = substr($lang, 0, 2);
                $langs[$short] = $langs[$lang];
            }

            arsort($langs, SORT_NUMERIC);
        }
    }

    return array_keys($langs);
}


/**
 * Return the prefered language.
 *
 * @param array $availables a list of available languages, must contain at
 *                          least one value.
 * @param array $preferences the list of prefered user languages.
 * @param string $default the default language to return.
 * @return string the first value from preferences which is also an available
 *                language, default value otherwise.
 */
function get_prefered_language($availables, $preferences, $default = null) {
    if (is_null($default)) {
        $default = current($availables);
    }

    foreach ($preferences as $lang) {
        if (isset($availables[$lang])) {
            return $availables[$lang];
        }
    }

    return $default;
}


/**
 * Initialize database with the given values.
 *
 * @param Pimple\Container $container a container wich contains database.
 * @param array $values an array which contains values to init databse.
 */
function init_database($container, $values) {
    // Update config
    $settings = array();
    if (isset($values['language'])) {
        $settings['application_language'] = $values['language'];
        unset($values['language']);
    }
    if (isset($values['url'])) {
        $settings['application_url'] = $values['url'];
        unset($values['url']);
    }
    if (isset($values['timezone'])) {
        $settings['application_timezone'] = $values['timezone'];
        unset($values['timezone']);
    }
    if (isset($values['currency'])) {
        $settings['application_currency'] = $values['currency'];
        unset($values['currency']);
    }
    if (isset($values['date_format'])) {
        $settings['application_date_format'] = $values['date_format'];
        unset($values['date_format']);
    }
    if (isset($values['datetime_format'])) {
        $settings['application_datetime_format'] = $values['datetime_format'];
        unset($values['datetime_format']);
    }
    $conf = new Kanboard\Model\ConfigModel($container);
    $conf->save($settings);

    // Update user database
    $user = new Kanboard\Model\UserModel($container);
    $values['id'] = 1;
    $user->update($values);
}


/**
 * Create a new account.
 *
 * This function creates files under PATH_ACCOUNTS and set the database with
 * correct values.
 * Note values must be verified before calling this function.
 *
 * @param string $subdomain subdomain of the account.
 * @param string $account name of the account to create.
 * @param string $password the account password.
 * @param string $email an email address.
 * @throws Exception if any error occured during process.
 */
function create_account($subdomain, $account, $password, $email) {
    $account_path = join_path(PATH_ACCOUNTS, $subdomain);
    mkdir($account_path);
    mkdir(join_path($account_path, 'files'));

    $available_languages = get_kanboard_languages();
    $prefered_languages = get_browser_languages();
    $default_language = in_array(LANGUAGE_DEFAULT, $available_languages) ?
                        LANGUAGE_DEFAULT :
                        $available_languages[0];
    $language = get_prefered_language($available_languages,
                                      $prefered_languages,
                                      $default_language);

    $values = array(
        // User
        'username' => $account,
        'name' => $account,
        'password' => $password,
        'email' => $email,
        // Settings
        'language' => $language,
        'url' => 'https://' . $subdomain . '.' . URL_BASE,
        'currency' => 'EUR',
        'timezone' => 'Europe/Paris',  // There is no reliable way to guess timezone so we force Paris.
        'date_format' => 'd/m/Y',
        'datetime_format' => 'd/m/Y H:i',
    );
    $container = get_kanboard_container($subdomain);
    init_database($container, $values);
}

/**
 * Set "rememberMe" cookie for autologin.
 *
 * @param string $subdomain The subdomain of the account.
 */
function set_autologin($subdomain) {
    $container = get_kanboard_container($subdomain);
    $remember_me_session = new Kanboard\Model\RememberMeSessionModel($container);
    $remember_me_cookie = new Kanboard\Core\Http\RememberMeCookie($container);

    $credentials = $remember_me_session->create(1, $remember_me_cookie->request->getIpAddress(), $remember_me_cookie->request->getUserAgent());
    // We set cookie manually so it can work for subdomains
    // Have a look to http://tools.ietf.org/html/rfc2109#section-4.3.2 for more information.
    setcookie(
        $remember_me_cookie::COOKIE_NAME,
        $remember_me_cookie->encode($credentials['token'], $credentials['sequence']),
        $credentials['expiration'],
        $remember_me_cookie->helper->url->dir(),
        get_main_domain(URL_BASE),
        $remember_me_cookie->request->isHTTPS(),
        true
    );
}

$test_values = true;
if (!is_writable(PATH_ACCOUNTS)) {
    $error = 'Le répertoire contenant les données doit pouvoir être écrit par le serveur web.';
} elseif (isset($_POST['subdomain_redirect'])) {
    // Handle account redirection.
    $subdomain = strtolower($_POST['subdomain_redirect']);
    if (check_account_exists($subdomain)) {
        header('Location: //' . $_POST['subdomain_redirect'] . '.' . URL_BASE);
    } else {
        $error_redirection = 'Ce compte ne semble pas exister. Êtes-vous sûr du nom de votre compte&nbsp;?';
    }
} elseif (!empty($_POST) && APP_OPENED) {
    $test_values = values_are_valid($_POST);

    $account_exists = true;
    $error = null;
    $subdomain = strtolower($_POST['subdomain']);
    $account = strtolower($_POST['account']);
    $password = $_POST['password'];
    $email = $_POST['email'];

    if ($test_values === true) {
        try {
            $account_exists = check_account_exists($subdomain);
        } catch (Exception $e) {
            $error = $e->getMessage();
        }
    } else {
        $error = 'Veuillez vérifier que les informations que vous avez entrées correspondent au format demandé.';
    }

    if ($account_exists === false) {
        // If $account_exists is false, there was no error catched earlier.
        try {
            create_account($subdomain, $account, $password, $email);
            set_autologin($subdomain);
        } catch (Exception $e) {
            $error = $e->getMessage();
        }
    }

    if (is_null($error) && $account_exists) {
        $error = 'Ce compte semble déjà exister, veuillez choisir un autre nom de compte.';
        $test_values = 'subdomain';
    } elseif (is_null($error)) {
        // Everything is fine, we can redirect user to its new board!
        header('Location: //' . $subdomain . '.' . URL_BASE);
    }
}

include 'views/index.phtml';
