<?php

session_start();

require '../../common.php';
require '../../kanboard/vendor/autoload.php';

class Paginator
{
    public $current_page = 1;
    public $items_per_page = 50;

    public function __construct($items)
    {
        $this->items = $items;
    }

    public function count()
    {
        return count($this->items);
    }

    public function items()
    {
        $items = array();
        $first_item = max($this->items_per_page * ($this->current_page - 1), 0);
        $last_item = min($first_item + $this->items_per_page, $this->count());
        for ($i = $first_item ; $i < $last_item ; $i++) {
            $items[] = $this->items[$i];
        }
        return $items;
    }

    public function iter()
    {
        $pages = array();
        for ($i = 0 ; $i < $this->number_pages() ; $i++) {
            $pages[] = $i + 1;
        }
        return $pages;
    }

    public function number_pages()
    {
        return intval(ceil($this->count() / $this->items_per_page));
    }

    public function current_page($page)
    {
        $this->current_page = min(max(1, $page), $this->count());
    }

    public function is_current_page($page)
    {
        return $this->current_page == $page;
    }

    public function previous_page()
    {
        return $this->current_page - 1;
    }

    public function has_previous_page()
    {
        return $this->previous_page() > 0;
    }

    public function next_page()
    {
        return $this->current_page + 1;
    }

    public function has_next_page()
    {
        return $this->next_page() <= $this->number_pages();
    }
}

class Flash implements \IteratorAggregate
{
    protected $keep_from_flush = array();
    protected $dont_keep_from_flush = false;

    public function __set($name, $value)
    {
        $_SESSION['_flash'][$name] = $value;
        if (!$this->dont_keep_from_flush) {
            $this->keep_from_flush[] = $name;
            $this->dont_keep_from_flush = false;
        }
    }

    public function getIterator()
    {
        return new \ArrayIterator($_SESSION['_flash']);
    }

    public function __construct()
    {
        if (empty($_SESSION['_flash'])) {
            $_SESSION['_flash'] = array();
        }
    }

    public function now()
    {
        $this->dont_keep_from_flush = true;
        return $this;
    }

    public function export()
    {
        return $_SESSION['_flash'];
    }

    public function flush()
    {
        foreach ($_SESSION['_flash'] as $key => $value) {
            if (!in_array($key, $this->keep_from_flush)) {
                unset($_SESSION['_flash'][$key]);
            }
        }
    }
}

function generate_secure_token()
{
    $token = bin2hex(openssl_random_pseudo_bytes(16));
    $_SESSION['token'] = $token;
    return $token;
}

function reset_password($account, $username, $password)
{
    $container = get_kanboard_container($account);
    $user_model = new Kanboard\Model\UserModel($container);
    $user = $user_model->getByUsername($username);
    return $user && $user_model->update(array(
        'id' => $user['id'],
        'password' => $password,
        'nb_failed_login' => 0,
        'lock_expiration_date' => 0,
    ));
}

$flash = new Flash();

$do = isset($_GET['do']) ? $_GET['do'] : '';
if (isset($_GET['token']) && $_GET['token'] !== $_SESSION['token']) {
    $error = 'Mauvais token (' . $_GET['token'] . ') !';
} elseif ($do === 'reset') {
    if (reset_password($_POST['account'], $_POST['username'], $_POST['newPassword'])) {
        $flash->success = 'Le mot de passe de ' . $_POST['username'] . ' (sur le compte ' . $_POST['account'] . ') a bien été modifié.';
    } else {
        $flash->danger = 'Le mot de passe de ' . $_POST['username'] . ' (sur le compte ' . $_POST['account'] . ') n\'a pas pu être modifié.';
    }
    header('Location: //' . URL_BASE . '/admin/?do=see&search=' . $_POST['account']);
} elseif ($do === 'delete') {
    rm_rf(join_path(PATH_ACCOUNTS, $_GET['account']));
    $flash->success = 'Le compte ' . $_GET['account'] . ' a bien été supprimé';
    header('Location: //' . URL_BASE . '/admin');
}

if ($do === 'see') {
    $container = get_kanboard_container($_GET['search']);
    $user_model = new Kanboard\Model\UserModel($container);
    $users = $user_model->getAll();
    foreach ($users as $key => $user) {
        $user['is_admin'] = $user_model->isAdmin($user['id']);
        $users[$key] = $user;
    }
}

if ($do === 'search' || $do === 'see') {
    $accounts = filter_accounts(isset($_GET['search']) ? $_GET['search'] : '');
} else {
    $accounts = list_accounts();
}

$paginator = new Paginator($accounts);
$paginator->current_page(isset($_GET['page']) ? intval($_GET['page']) : 1);
$token = generate_secure_token();

include 'layout.php';

$flash->flush();
